package com.example.workmanagerservice

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.restapi.ApiInterface
import com.example.restapi.DataAdapter
import com.example.restapi.MyDataItem
import com.example.restapi.MyService
import com.example.workmanagerservice.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val adapter = DataAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Intent(this, MyService::class.java).also {
            startService(it)
        }

        show_data.setOnClickListener() {
            getMyData()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Intent(this, MyService::class.java).also {
            stopService(it)
        }
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>,
                response: Response<List<MyDataItem>?>
            ) {
                val responseBody = response.body()!!
                var spisok = ArrayList<MyDataItem>()

                val myStringBuilder = StringBuilder()
                responseBody.forEach { myData ->
                    myStringBuilder.append(myData.id)
                    myStringBuilder.append(" ")
                    myStringBuilder.append(myData.title)
                    spisok.add(myData)
                }
                init(spisok)
            }

            override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
                Log.d("MainActivity", "onFailure"+t.message)
            }
        })
    }

    private fun init(spisok: ArrayList<MyDataItem>) {
        binding.apply {
            recycleView.layoutManager = LinearLayoutManager(this@MainActivity)
            recycleView.adapter = adapter
            adapter.addstroki(spisok)
        }
    }
}