package com.example.restapi

import android.content.Context
import android.os.SystemClock.sleep
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.restapi.databinding.ActivityMainBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UploadWorker(context: Context, parameters: WorkerParameters): Worker(context, parameters) {

    lateinit var binding: ActivityMainBinding
    private val adapter = DataAdapter()

    override fun doWork(): Result {
        try {
            while (true) {
                getMyData()
                Log.i("MYTAG", "Данные обновлены")
                sleep(1000)
            }
            return Result.success()
        } catch (e:Exception) {
            return Result.failure()
        }
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>,
                response: Response<List<MyDataItem>?>
            ) {
                val responseBody = response.body()!!
                var spisok = ArrayList<MyDataItem>()

                val myStringBuilder = StringBuilder()
                responseBody.forEach { myData ->
                    myStringBuilder.append(myData.id)
                    myStringBuilder.append(" ")
                    myStringBuilder.append(myData.title)
                    spisok.add(myData)
                }
            }

            override fun onFailure(call: Call<List<MyDataItem>?>, t: Throwable) {
                Log.d("MainActivity", "onFailure"+t.message)
            }
        })
    }
}